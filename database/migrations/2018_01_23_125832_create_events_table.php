<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->integer('status')->default(0);
            $table->dateTime('beginning');
            $table->dateTime('ending')->nullable();
            $table->text('recurring')->nullable();
            $table->text('description')->nullable();
            $table->text('location')->nullable();
            $table->text('city')->nullable();
            $table->char('uf', 2)->nullable();
            $table->text('keywords')->nullable();
            $table->text('cover')->nullable();
            $table->integer('enterprise_id')->unsigned()->nullable();
            $table->integer('type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('event_types')->onDelete('cascade');
            $table->foreign('enterprise_id')->references('id')->on('enterprises')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
