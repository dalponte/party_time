<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            [
                'id' => 1,
                'email' => 'admin@ad.min',
                'name' => 'Administrador',
                'is_admin' => 1,
                'password' => password_hash('def', PASSWORD_DEFAULT),
            ],
        ]);
    }
}
