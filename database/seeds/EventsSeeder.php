<?php

use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('events')->insert([
            [
                'title' => 'Baile funk de laje',
                'beginning' => '2018-02-05 23:01:00',
                'ending' => '2018-02-05 23:59:00',
                'description' => 'Descrição detalhada',
                'location' => 'Na laje do topo do morro',
                'city' => 'Quiçassedo do sul',
                'uf' => 'QÇ',
                'keywords' => 'baile, quiçassa, funk, laje',
                'enterprise_id' => NULL,
                'type_id' => 2,
                'user_id' => 1,
                'cover' => '/storage/covers/QVBIWdzOtCmhlfLTnqO3HmOTOlHm7caulc08mSQp.jpeg',
            ], [
                'title' => 'Show de rock paulera',
                'beginning' => '2018-02-15 18:01:00',
                'ending' => '2018-02-20 23:59:00',
                'description' => 'Descrição detalhada',
                'location' => 'No quinto dos infernos',
                'city' => 'Abraço do diabo',
                'uf' => 'IN',
                'keywords' => 'Inferno, demonios, rock, roll',
                'enterprise_id' => 2,
                'type_id' => 1,
                'user_id' => 1,
                'cover' => '/storage/covers/LLTFq2MxlcoYpOyQJt4hPb0irP3qk5mBOayYkGOj.jpeg',
            ], [
                'title' => 'Micareta da perdição',
                'beginning' => '2018-02-10 18:01:00',
                'ending' => '2018-02-10 23:59:00',
                'description' => 'Descrição detalhada',
                'location' => 'lugar que não deveria existir',
                'city' => 'rio de janeiro',
                'uf' => 'RJ',
                'keywords' => 'rio, de, janeiro, não, deveria, existir',
                'enterprise_id' => 1,
                'type_id' => 4,
                'user_id' => 1,
                'cover' => '/storage/covers/5XkekTqs2CdZlPzHYnjF2ckb524ed5H4vrlSMKrX.png',
            ]
        ]);
    }
}
