<?php

use Illuminate\Database\Seeder;

class EnterprisesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enterprises')->insert([
            [
                'id' => 1,
                'name' => 'Pavilhão da dona carmelinda',
                'address' => 'Rua da dona carmelinda',
                'location' => 'Rio de Janeiro',
                'user_id' => 1,
                'maps' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d32139.89999341839!2d-52.661162442639515!3d-26.22351942007853!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e5533f62e22c55%3A0x74c614e61d62d7ce!2sPato+Branco%2C+PR!5e0!3m2!1spt-BR!2sbr!4v1517675984232',
            ], [
                'id' => 2,
                'name' => 'Casa de show "Risca faca"',
                'address' => 'Quinto dos infernos 1666',
                'location' => 'Pindamoiangaba',
                'user_id' => 1,
                'maps' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d32139.89999341839!2d-52.661162442639515!3d-26.22351942007853!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e5533f62e22c55%3A0x74c614e61d62d7ce!2sPato+Branco%2C+PR!5e0!3m2!1spt-BR!2sbr!4v1517675984232',
            ]
        ]);
    }
}
