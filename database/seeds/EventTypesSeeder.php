<?php

use Illuminate\Database\Seeder;

class EventTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_types')->insert([
            ['title' => 'Festa', 'description' => 'Descrição completa'],
            ['title' => 'Baile', 'description' => 'Descrição completa'],
            ['title' => 'Matinê', 'description' => 'Descrição completa'],
            ['title' => 'Saral', 'description' => 'Descrição completa'],
            ['title' => 'Show', 'description' => 'Descrição completa'],
            ['title' => 'Rave', 'description' => 'Descrição completa'],
            ['title' => 'Mosh', 'description' => 'Descrição completa'],
            ['title' => 'Forró', 'description' => 'Descrição completa'],
            ['title' => 'Festival', 'description' => 'Descrição completa'],
            ['title' => 'Feira', 'description' => 'Descrição completa'],
            ['title' => 'Exposição', 'description' => 'Descrição completa'],
            ['title' => 'Congresso', 'description' => 'Descrição completa'],
        ]);
    }
}
