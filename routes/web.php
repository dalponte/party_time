<?php

Route::get('/', 'PagesController@index')->name('featured');
Route::get('/eventos', 'PagesController@eventos')->name('eventos');
Route::get('/evento/{event}', 'PagesController@evento')->name('evento');
Route::get('/estabelecimentos', 'PagesController@estabelecimentos')->name('estabelecimentos');
Route::get('/estabelecimento/{enterprise}', 'PagesController@estabelecimento')->name('estabelecimento');
Route::post('/set_city', 'PagesController@set_city')->name('set_city');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'PagesController@home')->name('home');
    Route::resource('/enterprises', 'EnterprisesController');
    Route::resource('/events', 'EventsController');
    Route::resource('/users', 'UsersController');
    Route::post('/events/{event}/status', 'EventsController@status')->name('events.status');
    Route::resource('/types', 'EventTypesController');
    Route::get('/profile', 'ProfileController@show')->name('profile');
    Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::put('/profile/', 'ProfileController@update')->name('profile.update');
});