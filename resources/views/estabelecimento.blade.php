@extends('template')

@section('content')

    <div class="d-flex justify-content-center my-3">
        @if($enterprise->logo)
            <div class="enterprise-logo d-flex justify-content-center align-content-center">
                <img class="enterprise-logo" src="{{$enterprise->logo}}">
            </div>
        @else
            <div class="enterprise-logo rounded-circle">
                <img class=" " src="/img/generic-establishment.jpg">
            </div>
        @endif
    </div>
    <div class="text-center font-custom">
        <h3 class="text-uppercase font-weight-bold">{{$enterprise->name}}</h3>
    </div>

    <div class="row mt-3">
        <div class="col-md-6">
            <hr>
            <div>
                <label class="text-muted"><i class="fa fa-location-arrow"></i> Local</label>
            </div>
            {{$enterprise->location}}
            <br>
            {{$enterprise->address}}
        </div>
        @if($enterprise->attendance)
            <div class="col-md-6">
                <hr>
                <div>
                    <label class="text-muted"><i class="fa fa-clock-o"></i> Atendimento</label>
                </div>
                {{$enterprise->attendance}}
            </div>
        @endif
        @if($enterprise->link)
            <div class="col-md-6">
                <hr>
                <div>
                    <label class="text-muted"><i class="fa fa-globe"></i> Site</label>
                </div>
                <a href="{{$enterprise->link}}">{{$enterprise->link}}</a>
            </div>
        @endif
        @if($enterprise->owner)
            <div class="col-md-6">
                <hr>
                <div>
                    <label class="text-muted"><i class="fa fa-user-secret"></i> Responsável</label>
                </div>
                {{$enterprise->owner}}
            </div>
        @endif

        @if($enterprise->owner)
            <div class="col-md-6">
                <hr>
                <div>
                    <label class="text-muted"><i class="fa fa-user-secret"></i> Responsável</label>
                </div>
                {{$enterprise->owner}}
            </div>
        @endif
    </div>
    @if($enterprise->maps)
        <div class="">
            <hr>
            <h5>Como chegar</h5>
            <iframe src="{{ $enterprise->maps }}" width="100%" height="300px"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    @endif

    <div class="my-3">
        <hr>
        <h5>Eventos deste estabelecimento</h5>
        <div class="row">
            @foreach($enterprise->events as $event)
                <div class="col-md-4 mb-1">
                    <div class="card" style="width: 25rem;">
                        @if($event->cover)
                            <img class="card-img-top" src="{{$event->cover}}" alt="Capa do evento">
                        @else
                            <img class="card-img-top" src="/img/generic-event-4.jpg" alt="Capa do evento">
                        @endif
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{route('evento', [$event])}}"> {{$event->title}}</a></h5>
                            <i class="fa fa-clock-o"></i> Realizado: {{$event->beginning}}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection