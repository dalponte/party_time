@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">Editar seu perfil</div>
        <div class="">
            <a href="/profile" class="btn btn-sm btn-outline-warning">
                <i class="fa fa-arrow-circle-left"></i> Voltar</a>
        </div>
    </div>

    <form method="POST" enctype="multipart/form-data" action="/profile">

        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="control-label">Nome *</label>

            <input id="name" type="text" class="form-control" name="name"
                   value="{{ old('name', $user->name) }}" placeholder="Nome Completo" required autofocus>

            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label">E-Mail *</label>

            <input id="email" type="email" class="form-control" name="email"
                   value="{{ old('email', $user->email) }}" placeholder="E-mail" disabled="disabled">

            @if ($errors->has('email'))
                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('curPassword') ? ' has-error' : '' }}">
            <label for="curPassword" class="control-label">Senha Atual</label>

            <input id="curPassword" type="password" class="form-control" name="curPassword"
                   placeholder="Deixe em branco para não mudar" >

            @if ($errors->has('curPassword'))
                <span class="help-block text-danger">{{ $errors->first('curPassword') }}</span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('newPassword') ? ' has-error' : '' }}">
            <label for="newPassword" class="control-label">Nova Senha</label>

            <input id="newPassword" type="password" class="form-control" name="newPassword"
                   placeholder="Deixe em branco para não mudar" >

            @if ($errors->has('newPassword'))
                <span class="help-block text-danger">{{ $errors->first('newPassword') }}</span>
            @endif
        </div>

        <div class="form-group">
            <label for="newPassword-confirm" class="control-label">Confirme a senha</label>
            <input id="newPassword-confirm" type="password" class="form-control"
                   name="newPassword_confirmation" placeholder="Deixe em branco para não mudar" >
        </div>

        {{method_field('PUT')}}
        {{csrf_field()}}
        <button type="submit" class="btn btn-primary">Enviar</button>

    </form>

@endsection