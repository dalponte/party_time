@extends('template')

@section('content')
    <?php $_user = Auth::user()?>
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">Seu pefil</div>
        <div>
            <a href="/profile/edit" class="btn btn-sm btn-outline-dark">
                <i class="fa fa-pencil"></i> Editar</a>
            <a href="/home/" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-bar-chart"></i> Visão geral</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php $fields = [
                'name',
                'email',
            ]?>
            <table class="table">
                @foreach($fields as $attr)
                    <tr class="">
                        <td class="text-capitalize font-weight-light text-muted text-right">
                            @lang('attributes.'.$attr):
                        </td>
                        <td>{{$user->$attr}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection
