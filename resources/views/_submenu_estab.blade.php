<?php
$sel_order = ['name-asc' => '', 'name-desc' => ''];
if (!isset($filter) || !count($filter)) {
    $filter = [
        'city' => session('user_location'),
        'name' => '',
        'order' => 'name-asc',
    ];
}
$sel_order[$filter['order']] = 'selected="selected"';
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler bg-warning" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="fa fa-filter" style="font-size: 1.1em"></span> Filtros
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <form method="get" action="{{route('estabelecimentos')}}" enctype="multipart/form-data">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                <li class="nav-item my-md-1">
                    <label class="small-label " for="filter-city"> Cidade: </label>
                    <input type="text" class="form-control-sm mx-1" placeholder="Filtrar por cidade"
                           id="filter-city" name="city" value="{{$filter['city']}}">
                </li>

                <li class="nav-iteventem my-md-1">
                    <label class="small-label " for="filter-name">Nome: </label>
                    <input type="text" class="form-control-sm mx-1" placeholder="Nome do evento"
                           id="filter-name" name="name" value="{{$filter['name']}}">
                </li>

                <li class="nav-item my-md-1">
                    <label class="small-label order" for="filter-order">Ordenar por </label>
                    <select class="form-control-sm mx-1" name="order" id="filter-order">
                        <option value="name-asc" {{$sel_order['name-asc']}}> A-Z</option>
                        <option value="name-desc" {{$sel_order['name-desc']}}> Z-A</option>
                    </select>
                </li>

                <li class="nav-item my-md-1">
                    <button type="submit" class="btn btn-sm btn-warning"> Filtrar</button>
                </li>

            </ul>
        </form>
    </div>
</nav>
<script>
    function initialize() {
        var autoCompleteInput = document.getElementById('filter-city');
        var autoCompleteOpcoes = {
            types: ['(cities)']
        };
        autocomplete = new google.maps.places.Autocomplete(autoCompleteInput, autoCompleteOpcoes);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>
