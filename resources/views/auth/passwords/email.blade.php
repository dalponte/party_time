@extends('template')

@section('content')

    <div class="card card-default mt-3">

        <img class="img-fluid" src="/img/generic-event-3.jpg"
             style="position: absolute;top: 0;width: 100%;">

        <div class="card-body">
            <div class="row">
                <div class="col-md-6 m-auto">
                    <div class="card">

                        <div class="card-body">
                            <h5>Redefinir senha</h5>
                            <p>Enviaremos um e-mail para que você possa redefinir sua senha</p>

                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-Mail</label>

                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Enviar link de redefinição de senha
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
