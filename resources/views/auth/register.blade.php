@extends('template')

@section('content')

    <div class="card card-default mt-3">

        <img class="img-fluid" src="/img/generic-event-2.jpg"
             style="position: absolute;top: 0;width: 100%;">

        <div class="card-body">
            <div class="row">
                <div class="hidden-md col-md-6 offset-md-1 d-flex align-items-center">
                    <div class="text-center">
                        <h3 class="text-warning text-sh-2">Crie uma conta e cadastre seus eventos</h3>
                        <p class="text-white text-sh-2">O cadastro é <b>gratuito </b> e permite que você cadastre
                            estabelecimentos e eventos. </p>
                        <b class="text-white text-sh-2">Já tem um cadastro?!</b>
                        <br>
                        <a class="btn btn-lg btn-warning mt-2" href="/login">Realizar login</a>
                    </div>
                </div>
                <div class="col-md-4 ml-auto">
                    <div class="card">
                        <div class="card-body">

                            <h5 class="text-center">Cadastre-se</h5>

                            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="control-label">Nome</label>

                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name') }}" placeholder="Nome Completo" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-Mail</label>

                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" placeholder="E-mail" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Senha</label>

                                    <input id="password" type="password" class="form-control" name="password"
                                           placeholder="Mínimo 6 caracteres" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="control-label">Confirme a senha</label>
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" placeholder="Confirme sua senha" required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-warning">
                                        Criar minha conta
                                    </button>
                                </div>
                            </form>

                            <div class="w-100 text-center">

                                <a class="btn btn-link" href="{{ route('login') }}">
                                    Realizar login
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
