@extends('template')

@section('content')

    <div class="card card-default mt-3">

        <img class="img-fluid" src="/img/generic-event-3.jpg"
             style="position: absolute;top: 0;width: 100%;">

        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-1 d-none d-md-flex d-lg-flex align-items-center">
                    <div class="text-center">
                        <h3 class="text-success text-sh-2">Bem vindo novamente</h3>
                        <p class="text-white text-sh-2">Realize seu login para gerenciar os eventos e
                            etabeleciomentos da sua conta</p>
                        <b class="text-white text-sh-2">Não está cadastrado?!</b>
                        <br>
                        <a class="btn btn-lg btn-success mt-2" href="/register">Cadastre-se agora</a>
                    </div>
                </div>
                <div class="col-md-4 ml-auto">
                    <div class="card">
                        <div class="card-body">

                            <h5 class="text-center">Login</h5>

                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-mail</label>

                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" placeholder="Seu e-mail" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Senha</label>


                                    <input id="password" type="password" class="form-control" name="password"
                                           placeholder="Sua senha" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">
                                        Login
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Esqueceu a senha?
                                    </a>
                                </div>
                            </form>
                            <div class="w-100 text-center">

                                <a class="btn btn-link" href="{{ route('register') }}">
                                    Cadastre-se
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
