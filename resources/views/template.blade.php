<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title><?= Config::get('app.name')?></title>
    <link rel="shortcut icon" href="<?= URL::asset('img/favicon.png')?>">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/datatables.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/tagsinput.css')}}">
    @yield('style')
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCm2Vs-D_RV2WS8-UAuqlPZ_YbWyEV9Vbk&libraries=places"></script>
</head>

<body>
<div class="container-fluid p-0">

    <header>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <a class="navbar-brand font-weight-bold" href="/"><i
                        class="fa fa-birthday-cake"></i> <?= Config::get('app.name')?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                    aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Destaques</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/eventos">Eventos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/estabelecimentos">Estabelecimentos</a>
                    </li>
                </ul>
                @if(Auth::check())
                    <?php $_user = Auth::user()?>
                    <div class="dropdown">
                        <button class="btn btn-outline-primary dropdown-toggle text-white" type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user"></i> {{$_user->name}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="/home"><i class="fa fa-bar-chart"></i>Visão geral</a>
                            <a class="dropdown-item" href="/profile"><i class="fa fa-user"></i>Perfil</a>
                            <form action="/logout" method="POST">
                                {{csrf_field()}}
                                <button type="submit" class="dropdown-item"><i class="fa fa-sign-out"></i>Sair</button>
                            </form>
                        </div>
                    </div>
                @else
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Cadastro</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Login</a>
                        </li>
                    </ul>
                @endif
            </div>
        </nav>

        @yield('submenu')

    </header>

    <main role="main" class="container">

        @yield('content')

    </main><!-- /.container -->

</div>
<script src="<?= URL::asset('js/jquery-3.2.1.min.js') ?>"></script>
<script src="<?= URL::asset('js/popper.min.js') ?>"></script>
<script src="<?= URL::asset('js/bootstrap.js') ?>"></script>
<script src="<?= URL::asset('js/jquery-renderOn-1.0.1.js') ?>"></script>
<script src="<?= URL::asset('js/datatables.js') ?>"></script>
<script src="<?= URL::asset('js/app.js') ?>"></script>
<script src="<?= URL::asset('js/tagsinput.js') ?>"></script>
@yield('js')

</body>
</html>
