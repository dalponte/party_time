@extends('template')

@section('submenu')
    @include('_submenu')
@endsection

@section('content')

    @if(!$events->count())
        <div class="alert alert-warning">
            Não foram encontrados eventos para a sua cidade. Veja outros eventos:
        </div>
        <?php
        $events = App\Event::orderBy('beginning')->get();
        ?>
    @endif

    <div class="row">
        @foreach($events as $n =>  $event)
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-card mb-3 px-2">
                <div class="card card-featured">
                    <div class="card-img-top img-container">
                        <img src="{{$event->cover}}" alt="Capa {{$event->title}}">
                    </div>
                    <div class="card-body">
                        <h6 class="card-title">{{$event->title}}</h6>
                        <p class="card-text">
                            {{$event->beginning}}<br>
                            {{$event->location}}<br>
                            {{$event->city}}<br>
                        </p>
                        <a href="/evento/{{$event->id}}" class="btn btn-secondary">Mais detalhes</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection