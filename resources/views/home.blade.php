@extends('template')

@section('content')
    <?php $_user = Auth::user()?>
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">Painel de controle</div>
        <div>
            @if($_user->is_admin)
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="/users" class="btn btn-sm btn-secondary">
                        <i class="fa fa-user"></i> Usuários</a>
                    <a href="/events" class="btn btn-sm btn-secondary">
                        <i class="fa fa-calendar"></i> Eventos
                    </a>
                    <a href="/enterprises" class="btn btn-sm btn-secondary">
                        <i class="fa fa-building"></i> Estabelecimentos
                    </a>
                </div>
            @endif

            <a href="/profile/" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-user"></i> Seu perfil</a>
        </div>
    </div>
    <div class="w-100 my-2 d-flex justify-content-between">
        <div class="h5"><i class="fa fa-calendar-times-o"></i> 5 próximos eventos</div>
        <div class="text-right">
            <a href="/events/" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-list"></i> Ver todos</a>
            <a href="/events/create" class="btn btn-sm btn-outline-primary">
                <i class="fa fa-plus-circle"></i> Novo evento</a>
        </div>
    </div>
    <div class="card mb-3">
        <div class=" card-body p-1">
            <table class="table table-striped">
                <tr>
                    <th>Início</th>
                    <th>Título</th>
                    <th>Cidade</th>
                    <th>Status</th>
                </tr>

                @foreach($events as $event)
                    <tr>
                        <td>
                            <small>{{$event->beginning}}</small>
                        </td>
                        <td><a href="/events/{{$event->id}}"> {{$event->title}}</a></td>
                        <td><?= "$event->city - <small>$event->uf</small>"?></td>
                        <td>
                            @if($event->status === -1)
                                <div class="text-danger">
                                    <i class="fa fa-warning"></i> Reprovado
                                </div>
                            @elseif($event->status)
                                <div class="text-success">
                                    <i class="fa fa-eye"></i> Publicado
                                </div>
                            @else
                                <div class="text-warning">
                                    <i class="fa fa-eye-slash"></i> Em avaliação
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach

            </table>

        </div>
    </div>
    <div class="w-100 my-2 d-flex justify-content-between">
        <div class="h5"><i class="fa fa-map-marker"></i> Seus Estabelecimentos</div>
        <div class="text-right">
            <a href="/enterprises/" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-list"></i> Ver todos</a>
            <a href="/enterprises/create" class="btn btn-sm btn-outline-primary">
                <i class="fa fa-plus-circle"></i> Novo Estabelecimento</a>
        </div>
    </div>
    <div class="card mb-3">
        <div class=" card-body p-1">
            <table class="table table-striped">
                <tr>
                    <th>Nome</th>
                    <th>Cidade</th>
                </tr>

                @foreach($enterprises as $ent)
                    <tr>
                        <td><a href="/enterprises/{{$ent->id}}"> {{$ent->name}} </a></td>
                        <td><?="$ent->city - <small>$ent->uf</small>"?></td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>

@endsection
