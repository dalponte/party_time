@extends('template')

@section('content')

    <div class="card card-default " style="padding-top: 100px">

        <img class="img-fluid" src="/img/generic-event-2.jpg"
             style="position: absolute;top: 0;width: 100%;">

        <div class="card-body">
            <div class="row">
                <div class="col-md-8 col-lg-6 m-auto">
                    <div class="card text-center">
                        <div class="card-body">
                            <h2 class="mb-3">Informe sua cidade </h2>
                            <p>Informe a sua localização para que encontremos os eventos mais próximos à você.</p>

                            <form class="form-inline" method="POST" action="{{ route('set_city') }}">
                                {{ csrf_field() }}

                                <div class="form-group m-auto">

                                    <input type="text" name="city" id="user-city" class="form-control"
                                           placeholder="Informe sua cidade">

                                    <button type="submit" class="btn btn-warning mx-1">
                                        Selecionar
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function initialize() {
            var autoCompleteInput = document.getElementById('user-city');
            var autoCompleteOpcoes = {
                types: ['(cities)']
            };
            autocomplete = new google.maps.places.Autocomplete(autoCompleteInput, autoCompleteOpcoes);
        }
        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
@endsection




