@extends('template')

@section('content')
    <div style="max-height: 400px; min-height: 100px; overflow: hidden" class="mt-3">
        @if($event->cover)
            <img src="{{$event->cover}}" class="img-fluid">
        @else
            <img src="/img/generic-event-1.jpg" class="img-fluid">
        @endif
    </div>
    <div class="d-block">
        <div class="card event-title mb-3">
            <div class="card-body">
                <h2 class="font-custom text-center text-uppercase">{{$event->title}}</h2>

                <div class="d-md-flex justify-content-between">
                    <div class="h5 event-title-date">
                        <small class="label text-muted">Início:</small>
                        {{$event->beginning}}
                    </div>
                    <div class="h5 event-title-date">
                        <small class="label text-muted">Termino:</small>
                        {{$event->ending}}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="label">Cidade</div>
            <div class="mb-3 event-attr">
                {{$event->city}} -
                <small>{{$event->uf}}</small>
            </div>

            <div class="label">Localização</div>
            <div class="mb-3 event-attr">
                {{$event->location}}
            </div>

            <div class="label">Tipo de evento</div>
            <div class="mb-3 event-attr">
                {{$event->type->title}}
            </div>

            <div class="label">Palavras chave</div>
            <div class="mb-3 event-attr">
                {{$event->keywords}}
            </div>

        </div>
        <div class="col-md-6 col-lg-8">
            <div class="label">Descrição</div>
            <p class="event-attr"><?= $event->description ?></p>
        </div>
    </div>
    @if($event->enterprise)
        <hr>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="label">Estabelecimento</div>
                <div class="h5 mb-3">{{$event->enterprise->name}}</div>
                <div class="label">Endereço</div>
                <p class="">
                    {{$event->enterprise->city}} - {{$event->enterprise->uf}}
                    <br>
                    {{$event->enterprise->address}}
                    <br>
                    {{$event->enterprise->cep}}
                    <br>
                    {{$event->enterprise->phone}}
                </p>
            </div>
            <div class="col-md-6 col-lg-8">
                <iframe src="{{$event->enterprise->maps}}" width="100%" height="300px"
                        frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    @endif
@endsection