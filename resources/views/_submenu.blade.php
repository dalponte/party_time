<?php
$types = App\EventType::all()->pluck('title', 'id');
$sel_types = array_fill_keys($types->keys()->toArray(), '');
$sel_order = ['beginning-asc' => '', 'title-asc' => '', 'title-desc' => ''];
if (!isset($filter) || !count($filter)) {
    $filter = [
        'city' => session('user_location'),
        'title' => '',
        'type' => 0,
        'order' => 'beginning-asc',
    ];
}
$sel_types[$filter['type']] = 'selected="selected"';
$sel_order[$filter['order']] = 'selected="selected"';
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler bg-warning" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="fa fa-filter" style="font-size: 1.1em"></span> Filtros
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <form method="get" action="/eventos" enctype="multipart/form-data">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                <li class="nav-item my-md-1">
                    <label class="small-label " for="filter-city"> Cidade: </label>
                    <input type="text" class="form-control-sm mx-1" placeholder="Filtrar por cidade"
                           id="filter-city" name="city" value="{{$filter['city']}}">
                </li>

                <li class="nav-iteventem my-md-1">
                    <label class="small-label " for="filter-title">Nome: </label>
                    <input type="text" class="form-control-sm mx-1" placeholder="Nome do evento"
                           id="filter-title" name="title" value="{{$filter['title']}}">
                </li>

                <li class="nav-item my-md-1">
                    <label class="small-label " for="filter-type">Filtrar por tipo </label>
                    <select class="form-control-sm mx-1" name="type" id="filter-type">
                        <option value="0"> Todos</option>
                        @foreach($types as $type_id => $type)
                            <option value="{{$type_id}}" {{$sel_types[$type_id]}}>
                                {{$type}}
                            </option>
                        @endforeach
                    </select>
                </li>

                <li class="nav-item my-md-1">
                    <label class="small-label order" for="filter-order">Ordenar por </label>
                    <select class="form-control-sm mx-1" name="order" id="filter-order">
                        <option value="beginning-asc" {{$sel_order['beginning-asc']}}> Mais próximo</option>
                        <option value="title-asc" {{$sel_order['title-asc']}}> A-Z</option>
                        <option value="title-desc" {{$sel_order['title-desc']}}> Z-A</option>
                    </select>
                </li>

                <li class="nav-item my-md-1">
                    <button type="submit" class="btn btn-sm btn-warning"> Filtrar</button>
                </li>

                @if(false)
                    @foreach(App\EventType::all() as $type)
                        <a class="p-2 text-muted" href="/eventos?type={{$type->id}}">{{$type->title}}</a>
                    @endforeach
                @endif

            </ul>
        </form>
    </div>
</nav>
<script>
    function initialize() {
        var autoCompleteInput = document.getElementById('filter-city');
        var autoCompleteOpcoes = {
            types: ['(cities)']
        };
        autocomplete = new google.maps.places.Autocomplete(autoCompleteInput, autoCompleteOpcoes);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>
