@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">Seus estabelecimentos</div>
        <div class="buttons">
            <a href="/enterprises/create" class="btn btn-sm btn-outline-primary">
                <i class="fa fa-plus-circle"></i> Novo estabelecimento</a>
            <a href="/home" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-bar-chart"></i> Visão geral</a>
        </div>
    </div>

    <table class="table table-striped datatable init" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Cidade</th>
            <th> Detalhes</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($enterprises as $enterprise)
            <tr>
                <td>{{$enterprise->name}}</td>
                <td>{{$enterprise->location }}</td>
                <td>
                    <div class="d-flex justify-content-start">
                        <img class="iconized" src="{{$enterprise->logo}}">
                        <ul style="font-size: 9pt;">
                            <li>{{$enterprise->owner}}</li>
                            <li>{{$enterprise->link}}</li>
                            <li>{{$enterprise->attendance}}</li>
                        </ul>
                    </div>
                </td>
                <td>
                    <a href="/enterprises/{{$enterprise->id}}" class="btn btn-sm btn-outline-secondary">
                        <i class="fa fa-search"></i></a>
                    <a href="/enterprises/{{$enterprise->id}}/edit" class="btn btn-sm btn-outline-secondary">
                        <i class="fa fa-pencil"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection