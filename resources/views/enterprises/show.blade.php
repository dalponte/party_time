@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">{{$enterprise->name}}</div>
        <div class="buttons">
            <a href="/enterprises" class="btn btn-outline-warning  btn-sm ">
                <i class="fa fa-arrow-circle-left"></i> Voltar</a>

            <a href="/enterprises/{{$enterprise->id}}/edit" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-pencil"></i> Editar</a>

            <button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal"
                    data-target="#deleteEventModal">
                <i class="fa fa-close"></i> Remover
            </button>

            <!-- Modal -->
            <div class="modal fade" id="deleteEventModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Excluir estabelecimento</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p> Tem certeza que deseja remover este estabelecimento?</p>
                            <p><strong> Obs: TODOS OS EVENTOS DESTE ESTABELECIMENTO SERÃO REMOVIDOS! </strong></p>
                            <p>Não será possível desfazer esta ação.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
                                Fechar
                            </button>
                            <form action="{{route('enterprises.destroy', [$enterprise])}}" method="POST">
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-close"></i> Excluir Estabelecimento
                                </button>
                                <?= method_field('DELETE') ?>
                                <?= csrf_field() ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php $fields = [
                'name',
                'address',
                'location',
                'attendance',
                'cep',
                'owner',
                'link',
            ]?>
            <table class="table">
                @foreach($fields as $attr)
                    <tr class="">
                        <td class="text-capitalize font-weight-light text-muted text-right">
                            @lang('attributes.'.$attr):
                        </td>
                        <td>{{$enterprise->$attr}}</td>
                    </tr>
                @endforeach
                <tr class="">
                    <td class="text-capitalize font-weight-light text-muted text-right">Logomarca</td>
                    <td><img src="{{$enterprise->logo}}" style="max-width: 200px"></td>
                </tr>
            </table>
        </div>
        @if($enterprise->maps)
            <div class="col-md-6">
                <h5>Como chegar: </h5>
                <iframe src="{{$enterprise->maps}}" width="100%" height="300px"
                        frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        @endif
    </div>
@endsection