@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">{{$enterprise->name}}</div>
        <div class="">
            <a href="/enterprises" class="btn btn-sm btn-outline-warning">
                <i class="fa fa-arrow-circle-left"></i> Voltar</a>
        </div>
    </div>

    @include('enterprises._form', ['enterprise' => $enterprise, 'action' => route('enterprises.update', $enterprise)])

@endsection
@section('js')
    <script>
        function initialize() {
            var autoCompleteInput = document.getElementById('location');
            var autoCompleteOpcoes = {
                types: ['(cities)']
            };
            autocomplete = new google.maps.places.Autocomplete(autoCompleteInput, autoCompleteOpcoes);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection