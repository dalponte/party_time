<?php
$_user = Auth::user();
?>
<form method="POST" enctype="multipart/form-data" action="{{$action}}">
    <div class="form-group">
        <label for="name">Nome *</label>
        <input type="text" class="form-control" id="title" name="name"
               value="{{old('name', $enterprise->name)}}" placeholder="Nome do estabelecimento">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('name') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="location">Endereço *</label>
        <input type="text" class="form-control" id="address" name="address"
               value="{{old('address', $enterprise->address)}}" placeholder="Endereço do estabelecimento">
        @if ($errors->has('address'))
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('address') }}</strong></span>
        @endif
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="location">Cidade *</label>
            <input type="text" class="form-control" id="location" name="location"
                   value="{{old('location', $enterprise->location)}}" placeholder="Cidade do estabelecimento">
            @if ($errors->has('location'))
                <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('location') }}</strong></span>
            @endif
        </div>
    </div>


    <div class="form-group">
        <label for="attendance">Atendimento </label>
        <input type="text" class="form-control" id="attendance" name="attendance"
               value="{{old('attendance', $enterprise->attendance)}}" placeholder="Terça a domingo, das 20 às 03h">
        @if ($errors->has('attendance'))
            <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('attendance') }}</strong></span>
        @endif
    </div>
    <div class="form-group">
        <label for="cep">CEP</label>
        <input type="text" class="form-control" id="cep" name="cep" maxlength="9"
               value="{{old('cep', $enterprise->cep)}}" placeholder="*****-***">
        @if ($errors->has('cep'))
            <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('cep') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="owner">Dono/Responsável</label>
        <input type="text" class="form-control" id="owner" name="owner"
               value="{{old('owner', $enterprise->owner)}}" placeholder="Nome do responsável pelo estabelecimento">
        @if ($errors->has('owner'))
            <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('owner') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="link">Site</label>
        <input type="text" class="form-control" id="link" name="link"
               value="{{old('link', $enterprise->link)}}" placeholder="Link externo do estabelecimento">
        @if ($errors->has('link'))
            <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('link') }}</strong></span>
        @endif
    </div>
    <div class="form-group">
        <label for="maps">Google Maps</label>
        <input type="text" class="form-control" id="maps" name="maps"
               value="{{old('maps', $enterprise->maps)}}" placeholder="Link de compartilhamento pelo google maps">
        @if ($errors->has('maps'))
            <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('maps') }}</strong></span>
        @endif
    </div>
    <div class="form-row">
        <div class="form-group col-12 col-sm-6 col-md-8">
            <label for="logo">Logomarca</label>
            <input type="file" class="form-control-file" id="logo" name="logo" accept=".png, .jpg, .jpeg"
                   value="{{old('logo')}}">
            @if ($errors->has('logo'))
                <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('logo') }}</strong></span>
            @endif
        </div>
        <div class="col-12 col-sm-6 col-md-8">
            @if($enterprise->logo)
                <img src="{{$enterprise->logo}}" class="w-100 img-responsive" style="max-width: 150px">
            @endif
        </div>
    </div>


    {{$enterprise->id ? method_field('PUT'): ''}}
    {{csrf_field()}}
    <button type="submit" class="btn btn-primary">Enviar</button>

</form>