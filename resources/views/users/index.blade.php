@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">Usuários</div>
        <div class="buttons">
            <a href="/home" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-bar-chart"></i> Visão geral</a>
        </div>
    </div>

    <table class="table table-striped datatable init" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <a href="/users/{{ $user->id }}" class="btn btn-sm btn-outline-secondary">
                        <i class="fa fa-search"></i></a>
                    <a href="/users/{{ $user->id }}/edit" class="btn btn-sm btn-outline-secondary disabled"
                       disabled="disabled">
                        <i class="fa fa-pencil"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection