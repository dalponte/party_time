@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">{{$user->name}}</div>
        <div class="buttons">
            <a href="/users" class="btn btn-outline-warning  btn-sm ">
                <i class="fa fa-arrow-circle-left"></i> Voltar</a>

            <a href="/users/{{$user->id}}/edit" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-pencil"></i> Editar</a>

            <button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal"
                    data-target="#deleteEventModal">
                <i class="fa fa-close"></i> Remover
            </button>

            <!-- Modal -->
            <div class="modal fade" id="deleteEventModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Excluir estabelecimento</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p> Tem certeza que deseja remover este usuário?</p>
                            <p>Não será possível desfazer esta ação.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
                                Fechar
                            </button>
                            <form action="{{route('users.destroy', [$user])}}" method="POST">
                                <button type="submit" class="btn btn-danger disabled" disabled="disabled">
                                    <i class="fa fa-close"></i> Excluir Usuário
                                </button>
                                <?= method_field('DELETE') ?>
                                <?= csrf_field() ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php $fields = [
                'name',
                'email',
                'phone',
            ];?>
            <table class="table">
                @foreach($fields as $attr)
                    <tr class="">
                        <td class="text-capitalize font-weight-light text-muted text-right">
                            @lang('attributes.'.$attr):
                        </td>
                        <td>{{$user->$attr}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-md-6">
            <hr>
            <h5>Eventos do usuário</h5>
            <ul class="list-group">
                @foreach($user->events as $event)
                    <li class="list-group-item">
                        <a href="{{route('events.show', [$event])}}">{{$event->title}}</a>
                        <br>
                        <small>{{$event->beginning}}</small>
                        <br>
                        <small>{{$event->location}}</small>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection