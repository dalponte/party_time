@extends('template')

@section('submenu')
    @include('_submenu_estab')
@endsection

@section('content')

    @if(!$enterprises->count())
        <div class="alert alert-warning">
            Não foram encontrados estabelecimentos para a sua cidade.
        </div>
        <?php
        $enterprises = App\Enterprise::orderBy('name')->get();
        ?>
    @endif

    @foreach($enterprises as $n =>  $enterprise)
        <div class="card card-featured my-2">
            <div class="card-body">
                <div class="d-flex">
                    <a href="{{route('estabelecimento', $enterprise->id)}}" class=""
                       style="width: 200px; height: 200px; overflow: hidden">
                        @if($enterprise->logo)
                            <img src="{{$enterprise->logo}}" style="height: 200px; width: auto">
                        @else
                            <img src="/img/generic-project.jpg" style="height: 200px; width: auto">
                        @endif
                    </a>"
                    <div class="px-3">
                        <a href="{{route('estabelecimento', $enterprise->id)}}"
                           class="card-title"> {{$enterprise->name}}</a>

                        <div class="d-flex justify-content-between p-1 mt-2">
                            <div class="enterprise-location">
                                <i class="fa fa-map-marker text-muted"></i> {{$enterprise->location}}
                            </div>
                        </div>
                        <div class="w-100 p-1">
                            <i class=" fa fa-thumb-tack text-muted"></i> {{$enterprise->address}}
                        </div>
                        @if($enterprise->url)
                            <div class="w-100 p-1">
                                <i class=" fa fa-globe text-muted"></i> {{$enterprise->url}}
                            </div>
                        @endif
                        <hr class="m-1">
                        <i class="text-muted">
                            <small>Sem imagens</small>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection