@extends('template')

@section('submenu')
    @include('_submenu')
@endsection

@section('content')
    <div id="featured" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <?php
            $featured = App\Event::orderBy('beginning')->limit(3)->get()
            ?>
            @foreach($featured  as $n =>  $event)
                <a href="/evento/{{$event->id}}" class="carousel-item {{$n === 0? 'active': ''}}">
                    <img src="{{$event->cover}}" alt="">
                    <div class="carousel-caption d-none d-md-block">
                    </div>
                </a>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#featured" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#featured" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    @if(!$events->count())
        <div class="alert alert-warning">
            Não foram encontrados eventos para a sua cidade. Veja outros eventos:
        </div>
        <?php
        $events = App\Event::orderBy('beginning')->get();
        ?>
    @endif
    <div class="row">
        @foreach($events as $n =>  $event)
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-card mb-3 px-2">
                <div class="card card-featured">
                    <div class="card-img-top img-container">
                        <img src="{{$event->cover}}" alt="Capa {{$event->title}}">
                    </div>
                    <div class="card-body">
                        <h6 class="card-title">{{$event->title}}</h6>
                        <p class="card-text">
                            {{$event->beginning}}<br>
                            {{$event->location}}<br>
                            {{$event->city}}<br>
                        </p>
                        <a href="/evento/{{$event->id}}" class="btn btn-secondary">Mais detalhes</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
@section('js')
    <script>
    </script>
@endsection