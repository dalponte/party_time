@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">Novo evento</div>
        <div class="">
            <a href="/events" class="btn btn-sm btn-outline-warning">
                <i class="fa fa-arrow-circle-left"></i> Voltar</a>
        </div>
    </div>

    @include('events._form', ['event' => $event, 'action' => route('events.store')])

@endsection
@section('js')
    <script>
        function initialize() {
            var autoCompleteInput = document.getElementById('city');
            var autoCompleteOpcoes = {
                types: ['(cities)']
            };
            autocomplete = new google.maps.places.Autocomplete(autoCompleteInput, autoCompleteOpcoes);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection