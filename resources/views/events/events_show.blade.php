@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">{{$event->title}}</div>
        <div class="buttons">
            <a href="/events" class="btn btn-sm btn-outline-warning">
                <i class="fa fa-arrow-circle-left"></i> Voltar</a>
            <a href="/evento/{{$event->id}}/" class="btn btn-sm btn-outline-primary">
                <i class="fa fa-eye"></i> Ver evento</a>
            <a href="/events/{{$event->id}}/edit" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-pencil"></i> Editar</a>

            <button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal"
                    data-target="#deleteEventModal">
                <i class="fa fa-close"></i> Remover
            </button>

            <!-- Modal -->
            <div class="modal fade" id="deleteEventModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Excluir evento</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p> Tem certeza que deseja remover este evento?</p>
                            <p>Não será possível desfazer esta ação.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
                                Fechar
                            </button>
                            <form action="{{route('events.destroy', [$event])}}" method="POST">
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-close"></i> Excluir Evento
                                </button>
                                <?= method_field('DELETE') ?>
                                <?= csrf_field() ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if($event->status == 0)
        <div class="alert alert-warning">
            Este evento ainda não foi aprovado pela administração. Até ser aprovado, o evento não será exibido
            na plataforma.
        </div>
    @elseif($event->status == -1)
        <div class="alert alert-danger">
            Este evento foi rejeitado pela administração. Se houver dúvidas, entre em contato para amiores
            esclarecimentos.
        </div>
    @endif

    <?php $user = Auth::user() ?>
    @if($user->is_admin)
        <div class="card bg-light mb-3">
            <div class="card-header"><strong><i class="fa fa-user-secret"></i> Administrativo: Aprovação de evento</strong></div>
            <div class="card-block p-2">
                <?php $checked = [-1 => '', 1 => '', 0 => ''];
                $checked[$event->status] = 'checked="checked"'?>
                <div class="d-flex justify-content-between">
                    <div class="custom-radio">
                        <input type="radio" name="status" id="status-disabled" class="status-field" ]
                               value="0" data-id="{{$event->id}}" data-status="0" <?= $checked[0]?>>
                        <label for="status-disabled"> Não visível</label>
                    </div>
                    <div class="custom-radio">
                        <input type="radio" name="status" id="status-enabled" class="status-field"
                               value="1" data-id="{{$event->id}}" data-status="1" <?= $checked[1]?>>
                        <label for="status-enabled"> Aprovado</label>
                    </div>
                    <div class="custom-radio">
                        <input type="radio" name="status" id="status-rejected" class="status-field"
                               value="-1" data-id="{{$event->id}}" data-status="-1" <?= $checked[-1]?>>
                        <label for="status-rejected"> Reprovado</label>
                    </div>
                </div>
                <div class="w-100 " id="statusResultBox">

                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6">
            <?php $fields = [
                'title',
                'beginning',
                'ending',
                'description',
                'location',
                'city',
                'keywords',
            ]; ?>
            <table class="table">
                @foreach($fields as $attr)
                    <tr class="">
                        <td class="text-capitalize font-weight-light text-muted text-right">
                            @lang('attributes.'.$attr)
                        </td>
                        <td>{{$event->$attr}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-md-6">
            <img class="img-responsive w-100" src="{{$event->cover}}">
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            $('.status-field').on('change', function (ev) {
                $(ev.target).addClass('disabled');
                $(ev.target).attr('disabled', true);
                $('#statusResultBox').html('');
                $.ajax({
                    method: "POST",
                    url: "/events/" + $(this).data('id') + '/status',
                    data: {
                        id: $(this).data('id'), status: $(this).data('status'), '_token': '{{ csrf_token() }}'
                    }
                }).done(function (data) {
                    $('#statusResultBox').html(data.message);
                }).fail(function () {
                    alert("ERRO: Ocorreu um problema ao mudar a visibilidade");
                }).always(function () {
                    $(ev.target).removeClass('disabled');
                    $(ev.target).attr('disabled', false);
                });
            })
        });
    </script>
@endsection