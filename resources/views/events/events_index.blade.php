@extends('template')

@section('content')
    <div class="w-100 d-flex justify-content-between my-3">
        <div class="h5">Seus eventos</div>
        <div class="buttons">
            <a href="/events/create" class="btn btn-sm btn-outline-primary">
                <i class="fa fa-plus-circle"></i> Novo evento</a>
            <a href="/home" class="btn btn-sm btn-outline-secondary">
                <i class="fa fa-bar-chart"></i> Visão geral</a>
        </div>
    </div>

    <table class="table table-striped">
        <tr>
            <th>Início</th>
            <th>Título</th>
            <th>Cidade</th>
            <th>Local</th>
            <th>Ações</th>
        </tr>

        @foreach($events as $event)
            <tr>
                <td>
                    <small>{{$event->beginning}}</small>
                </td>
                <td>{{$event->title}}</td>
                <td><?= "$event->city - <small>$event->uf</small>"?></td>
                <td>{{$event->location}}</td>
                <td>
                    <a href="/events/{{$event->id}}" class="btn btn-sm btn-outline-secondary">
                        <i class="fa fa-search"></i></a>
                    <a href="/events/{{$event->id}}/edit" class="btn btn-sm btn-outline-secondary">
                        <i class="fa fa-pencil"></i></a>
                </td>
            </tr>
        @endforeach

    </table>
@endsection
