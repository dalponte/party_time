<?php
$_user = Auth::user();
$houses = $_user->enterprises;
$types = \App\EventType::all();

$sel_houses = array_fill_keys($houses->pluck('id')->toArray(), '');
$sel_houses[$event->enterprise_id] = 'selected="selected"';
$sel_types = array_fill_keys($types->pluck('id')->toArray(), '');
$sel_types[$event->type_id] = 'selected="selected"';
$beginning = (new \Carbon\Carbon($event->beginning))->formatLocalized('%Y-%m-%dT%H:%M');
$ending = (new \Carbon\Carbon($event->ending))->formatLocalized('%Y-%m-%dT%H:%M');
?>
<form method="POST" enctype="multipart/form-data" action="{{$action}}">
    <div class="form-group">
        <label for="title">Título do evento</label>
        <input type="text" class="form-control" id="title" name="title"
               value="{{old('title', $event->title)}}" placeholder="Nome do evento">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('title') }}</strong></span>
        @endif
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="beginning">Início</label>
            <input type="datetime-local" class="form-control" id="beginning" name="beginning"
                   value="{{old('beginning', $beginning)}}">
            @if ($errors->has('beginning'))
                <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('beginning') }}</strong></span>
            @endif
        </div>
        <div class="form-group col">
            <label for="ending">Termino</label>
            <input type="datetime-local" class="form-control" id="ending" name="ending"
                   value="{{old('ending', $ending)}}">
            @if ($errors->has('ending'))
                <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('ending') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="enterprise_id">Estabelecimento</label>
            <select class="form-control" id="enterprise_id" name="enterprise_id">
                <option value="" class="text-muted">Nenhum</option>
                @foreach($houses as $h)
                    <option value="{{$h->id}}" {{$sel_houses[$event->enterprise_id]}}>{{$h->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col">
            <label for="type_id">Tipo</label>
            <select class="form-control" id="type_id" name="type_id">
                @foreach($types as $t)
                    <option value="{{$t->id}}" {{$sel_types[$event->type_id]}}>{{$t->title}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="location">Local do evento</label>
        <input type="text" class="form-control" id="location" name="location"
               value="{{old('location', $event->location)}}" placeholder="Local do evento">
        @if ($errors->has('location'))
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('location') }}</strong></span>
        @endif
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="city">Cidade</label>
            <input type="text" class="form-control" id="city" name="city"
                   value="{{old('city', $event->city)}}" placeholder="Cidade do evento">
            @if ($errors->has('city'))
                <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('city') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="keywords">Palavras chave</label>
            <input type="text" class="form-control" id="keywords" name="keywords" data-role="tagsinput"
                   value="{{old('keywords', $event->keywords)}}" placeholder="Palavras chave ">
            @if ($errors->has('keywords'))
                <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('keywords') }}</strong></span>
            @endif
            <small id="keywordsHelp" class="form-text text-muted">
                Utilize vírgulas <code>,</code> para separar cada palavra-chave
            </small>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-12 col-sm-6 col-md-8">
            <label for="cover">Capa</label>
            <input type="file" class="form-control-file" id="cover" name="cover" accept=".png, .jpg, .jpeg"
                   value="{{old('cover')}}">
            @if ($errors->has('cover'))
                <span class="help-block">
                    <strong class="text-danger">{{ $errors->first('cover') }}</strong></span>
            @endif
        </div>
        <div class="col-12 col-sm-6 col-md-8">
            @if($event->cover)
                <img src="{{$event->cover}}" class="w-100 img-responsive" style="max-width: 150px">
            @endif
        </div>
    </div>

    <div class=" form-group">
        <label for="description">Descrição</label>
        <textarea class="form-control" id="description" name="description" rows="3"
                  placeholder="Descrição completa">{{$event->description}}</textarea>
    </div>

    {{$event->id ? method_field('PUT'): ''}}
    {{csrf_field()}}
    <button type="submit" class="btn btn-primary">Enviar</button>

</form>