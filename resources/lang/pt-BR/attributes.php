<?php

return [
    'name' => 'nome',
    'address' => 'endereço',
    'city' => 'cidade',
    'uf' => 'UF',
    'cep' => 'CEP',
    'owner' => 'dono',
    'link' => 'link',
    'maps' => 'maps',
    'id' => 'identificador',
    'title' => 'título',
    'beginning' => 'inicio',
    'ending' => 'termino',
    'description' => 'descrição',
    'location' => 'local',
    'keywords' => 'palavras-chave',
    'cover' => 'capa',
    'email' => 'E-mail',
    'attendance' => 'Atendimento',
];
