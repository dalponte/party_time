<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\SaveEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Storage;
use Image;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $events = Event::all();
        return view('events.events_index')->with(['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = new Event();
        return view('events.events_create')->with(['event' => $event]);
    }

    /**
     * @param SaveEvent $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(SaveEvent $request)
    {
        $event = new Event($request->all());
        $user = Auth::user();
        if ($request->hasFile('cover')) {
            $file = $request->file('cover');

            $path = $file->hashName('public/covers');

            $image = Image::make($file);
            $image->fit(1200, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::put($path, (string)$image->encode());
            $event->cover = Storage::url($path);
        }

        $event->status = 0;
        $event->user_id = $user->id;
        $event->save();
        return redirect(route('events.show', $event));
    }

    /**
     * Display the specified resource.
     *
     * @param  Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return view('events.events_show')->with(['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Event $event
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function edit(Event $event)
    {
        $user = Auth::user();
        if ($user->id != $event->user_id && !$user->is_admin)
            throw new \Exception('Você não pode visualizar esta página');
        return view('events.events_edit')->with(['event' => $event]);
    }

    /**
     * @param SaveEvent $request
     * @param Event $event
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(SaveEvent $request, Event $event)
    {
        $user = Auth::user();
        if ($user->id != $event->user_id)
            throw new \Exception('VOcê não pode visualizar esta página');
        $event->user_id = $user->id;
        $event->title = $request->input('title');
        $event->beginning = $request->input('beginning');
        $event->ending = $request->input('ending');
        $event->description = $request->input('description');
        $event->location = $request->input('location');
        $event->city = $request->input('city');
        $event->keywords = $request->input('keywords');
        $event->enterprise_id = $request->input('enterprise_id');
        $event->type_id = $request->input('type_id');

        if ($request->hasFile('cover')) {
            $file = $request->file('cover');

            $path = $file->hashName('public/covers');

            $image = Image::make($file);
            $image->fit(1200, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::put($path, (string)$image->encode());
            $event->cover = Storage::url($path);
        }
        $result = $event->save();
        return redirect(route('events.show', $event));
    }

    /**
     * @param Event $event
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Event $event)
    {
        $event->delete();
        return redirect(route('events.index'))
            ->with(['message' => 'Evento removido com sucesso']);
    }

    public function status(Request $request, Event $event)
    {
        $event->status = $request->status;
        $res = $event->save();
        return response()->json(['message' => '<b>Status atualizado</b>', 'result' => $res]);
    }

}
