<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    /**
     * Eventos em destaque
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q = Event::where('status', '1')->orderBy('beginning');

        $location = session('user_location');
        if ($location):
            $city = explode(',', $location);
            $city = is_array($city) ?
                str_replace(' ', '%', $city[0]) :
                str_replace(' ', '%', $location);
            $q->whereRaw('LOWER(`city`) LIKE LOWER(?)', array($city . '%'));
            $events = $q->get();
            return view('featured')->with(['events' => $events]);
        else:
            return view('select_city');
        endif;
    }

    /**
     * Página inicial do usuário
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $user = Auth::user();
        if ($user->is_admin)
            $events = Event::orderBy('beginning', 'ASC')->limit(5)->get();
        else
            $events = $user->events()->orderBy('beginning', 'ASC')->limit(5)->get();
        $enterprises = $user->enterprises()->limit(5)->get();
        return view('home')->with(['events' => $events, 'enterprises' => $enterprises]);
    }

    public function eventos(Request $request)
    {
        $location = session('user_location');
        $q = Event::where('status', '1');
        if ($request->has('type') && $request->type > 0)
            $q->where('type_id', $request->type);
        if ($request->has('city') && $request->city):
            $city = explode(',', $request->city);
            $city = is_array($city) ?
                str_replace(' ', '%', $city[0]) :
                str_replace(' ', '%', $request->city);
            $q->whereRaw('LOWER(`city`) LIKE LOWER(?)', array($city . '%'));
        endif;
        if ($request->has('title') && $request->title)
            $q->where('title', 'like', "%$request->title%");
        if ($request->has('uf') && $request->uf)
            $q->where('uf', $request->uf);
        if ($request->has('order')) {
            if ($request->order == 'title-asc')
                $q->orderBy('title', 'ASC');
            elseif ($request->order == 'title-desc')
                $q->orderBy('title', 'DESC');
            else {
                $q->orderBy('beginning', 'ASC');
            }
        }
        $events = $q->get();
        return view('eventos')->with([
            'events' => $events,
            'filter' => $request->all()
        ]);
    }

    public function evento(Event $event)
    {
        if (!$event->status)
            return redirect('/eventos')->with(['message' => 'O evento que você selecionou não pode ser encontrado']);
        return view('evento')->with(['event' => $event]);
    }

    public function set_city(Request $request)
    {
        $city_b = session('user_location');
        $request->session()->put('user_location', $request->city);

        $city = session('user_location');

        return redirect('/');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function estabelecimentos(Request $request)
    {
        $location = session('user_location');
        $q = Enterprise::query();
        if ($request->has('city') && $request->city):
            $city = explode(',', $request->city);
            $city = is_array($city) ?
                str_replace(' ', '%', $city[0]) :
                str_replace(' ', '%', $request->city);
            $q->whereRaw('LOWER(`city`) LIKE LOWER(?)', array($city . '%'));
        endif;
        if ($request->has('name') && $request->title)
            $q->where('name', 'like', "%$request->name%");
        if ($request->has('order')) {
            if ($request->order == 'name-asc')
                $q->orderBy('name', 'ASC');
            elseif ($request->order == 'name-desc')
                $q->orderBy('name', 'DESC');
        }
        $enterprises = $q->get();
        return view('estabelecimentos')->with([
            'enterprises' => $enterprises,
            'filter' => $request->all()
        ]);
    }

    /**
     * @param Request $request
     * @param Enterprise $enterprise
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function estabelecimento(Request $request, Enterprise $enterprise)
    {
        return view('estabelecimento')->with(['enterprise' => $enterprise]);
    }

}
