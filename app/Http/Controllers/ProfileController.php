<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        return view('profile.show')->with(['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        return view('profile.edit')->with(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $messages = [
            'name.required' => 'Informe seu nome!',
            'email.required' => 'Informe um e-mail para realizar o login',
            'email.email' => 'Informe um email válido',
            'email.unique' => 'Este e-mail já é utilizado',
            'newPassword.required' => 'Informe uma senha',
            'newPassword.min' => 'Utilize uma senha com pelo menos 6 caracteres',
            'newPassword.confirmed' => 'As senhas não conferem',
        ];
        $request->validate([
            'name' => 'required|string|max:255',
            'curPassword' => 'nullable',
            'newPassword' => 'nullable|string|min:6|confirmed',
        ], $messages);
        $user = Auth::user();
        $user->name = $request->name;
        if ($request->curPassword) {
            $curPassword = $request->curPassword;
            $newPassword = $request->newPassword;

            if (Hash::check($curPassword, $user->password)):
                $request->validate([
                    'newPassword' => 'required|string|min:6|confirmed',
                ], $messages);
                $user->password = Hash::make($newPassword);
            else:
                return back()->withErrors(['curPassword' => 'Senha não confere']);
            endif;

        }
        $user->save();
        return redirect('/profile');
    }

}
