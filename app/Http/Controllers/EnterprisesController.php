<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\Http\Requests\SaveEnterprise;
use Illuminate\Http\Request;
use Auth;
use Storage;
use Image;

class EnterprisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->is_admin):
            $enterprises = Enterprise::all();
        else:
            $enterprises = $user->enterprises;
        endif;
        return view('enterprises.index')->with(['enterprises' => $enterprises]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $enterprise = new Enterprise();
        $enterprise->user_id = $user->id;
        return view('enterprises.create')->with(['enterprise' => $enterprise]);
    }

    /**
     * @param SaveEnterprise $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveEnterprise $request)
    {
        $user = Auth::user();
        $enterprise = new Enterprise($request->all());
        $enterprise->user_id = $user->id;

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');

            $path = $file->hashName('public/logos');

            $image = Image::make($file);
            $image->fit(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::put($path, (string)$image->encode());
            $enterprise->logo = Storage::url($path);
        }

        $enterprise->save();
        return redirect(route('enterprises.show', $enterprise));
    }

    /**
     * Display the specified resource.
     *
     * @param  Enterprise $enterprise
     * @return \Illuminate\Http\Response
     */
    public function show(Enterprise $enterprise)
    {
        return view('enterprises.show')->with(['enterprise' => $enterprise]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Enterprise $enterprise
     * @return \Illuminate\Http\Response
     */
    public function edit(Enterprise $enterprise)
    {
        return view('enterprises.edit')->with(['enterprise' => $enterprise]);
    }

    /**
     * Update the specified resource in storage.
     * @param SaveEnterprise $request
     * @param  Enterprise $enterprise
     * @return \Illuminate\Http\Response
     */
    public function update(SaveEnterprise $request, Enterprise $enterprise)
    {
        $enterprise->fill($request->all());

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');

            $path = $file->hashName('public/logos');

            $image = Image::make($file);
            $image->fit(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::put($path, (string)$image->encode());
            $enterprise->logo = Storage::url($path);
        }

        $enterprise->save();
        return redirect(route('enterprises.show', $enterprise));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Enterprise $enterprise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enterprise $enterprise)
    {
        $enterprise->delete();
        return redirect(route('enterprises.index'))
            ->with(['message' => 'Estabelecimento removido com sucesso']);
    }
}
