<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveEnterprise extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'location' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Informe  o nome do estabelecimento!',
            'address.required' => 'Informe o endereço do estabelecimento!',
            'location.required' => 'Informe a cidade do estabelecimento',
        ];
    }
}
