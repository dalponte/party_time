<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'beginning' => 'required|date',
            'ending' => 'nullable|date||after:beginning',
            'cover' => 'nullable|image',
            'city' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Você precisa definir um título para o evento!',
            'beginning.required' => 'Informe a data em que o evento começa!',
            'beginning.date' => 'A data de início não é válida',
            'ending.datebeginning' => 'A data de término não é válida',
            'ending.after' => 'Informe uma data posterior ao início',
            'cover.image' => 'O arquivo selecionado deve ser uma imagem',
            'city.required' => 'Informe a cidade do evento',
            'uf.required' => 'Informe a cidade do evento',
            'uf.min' => 'Informe dois caracteres',
        ];
    }
}
