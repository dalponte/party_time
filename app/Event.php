<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 * @package App
 * @property integer id
 * @property string title
 * @property integer status
 * @property Carbon beginning
 * @property Carbon ending
 * @property string description
 * @property string location
 * @property string city
 * @property string keywords
 * @property integer enterprise_id
 * @property integer type_id
 * @property integer user_id
 */
class Event extends Model
{
    protected $fillable = [
        'title',
        'status',
        'beginning',
        'ending',
        'description',
        'city',
        'location',
        'keywords',
        'enterprise_id',
        'type_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise');
    }

    public function type()
    {
        return $this->belongsTo('App\EventType');
    }
}
