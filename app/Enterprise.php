<?php

namespace App;

/**
 * @property integer id
 * @property string name
 * @property string address
 * @property string location
 * @property string attendance
 * @property string cep
 * @property string owner
 * @property string link
 * @property string maps
 * @property string logo
 */
use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    protected $fillable = [
        'name',
        'address',
        'location',
        'attendance',
        'cep',
        'owner',
        'link',
        'maps',
        'logo',
        'user_id',
    ];

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
